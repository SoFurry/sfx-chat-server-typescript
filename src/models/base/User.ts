/*
 * TypeChat NodeJS Chat Engine
 *
 * @link https://gitlab.com/terrabas/TypeChat
 * @copyright Copyright (c) 2018-2018 Martin Schwarz (TerraBAS) (http://mrtns.at)
 */

import {Character} from "../Character";

export class User {
    id:number;
    username:string;
    character_id:number;
    character:Character;

    constructor(data){
        this.id = data.id;
        this.username = data.username;
        this.character_id = data.character_id;
        this.character = null;
    }
}