/*
 * TypeChat NodeJS Chat Engine
 *
 * @link https://gitlab.com/terrabas/TypeChat
 * @copyright Copyright (c) 2018-2018 Martin Schwarz (TerraBAS) (http://mrtns.at)
 */

import {ConsoleLog} from "../helpers/ConsoleLog";
import * as socketio from "socket.io";
import {TypeChatServer} from "./TypeChatServer";

export class ConnectionHandler {
    socket:any;
    Logger:ConsoleLog;
    constructor(public server:TypeChatServer){
        this.Logger = new ConsoleLog("ConnectionHandler");
        this.Logger.info("Preparing socket...");
    }

    start(){
        this.Logger.info("Starting socket...");
        this.socket = socketio(this.server.ServerConfig.port);
        this.socket.set('origins','*:*');
        this.bind('connection',this.onConnection.bind(this));
    }

    bind(command:string, func:any){
        this.socket.on(command, func.bind(this));
    }

    onConnection(clientSocket:any){
        this.server.moduleManager.clientModule.createNewClient(clientSocket, this.server);
    }
}