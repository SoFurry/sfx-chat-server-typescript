/*
 * TypeChat NodeJS Chat Engine
 *
 * @link https://gitlab.com/terrabas/TypeChat
 * @copyright Copyright (c) 2018-2018 Martin Schwarz (TerraBAS) (http://mrtns.at)
 */

import {BasePermissions} from "../modules/base/BasePermissions";

export class PermissionManager{
    static check;

    static loadRules(permissions:BasePermissions){
        this.check = permissions;
    }
}