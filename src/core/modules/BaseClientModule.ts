/*
 * TypeChat NodeJS Chat Engine
 *
 * @link https://gitlab.com/terrabas/TypeChat
 * @copyright Copyright (c) 2018-2018 Martin Schwarz (TerraBAS) (http://mrtns.at)
 */

import {BaseModuleInterface} from "./BaseModuleInterface";
import {User} from "../../models/base/User";
import {TypeChatServer} from "../TypeChatServer";
import {BaseClient} from "../../modules/base/BaseClient";
import {BaseMessage} from "../../models/base/BaseMessage";

export interface BaseClientModule extends BaseModuleInterface {
    /**
     * Array that holds all clients
     */
    clients:BaseClient[];

    /**
     * Gets called when a client has successfully authenticated for further processing
     * @param {BaseClient} client
     */
    register(client:BaseClient);

    /**
     * Returns if a client has already been registered
     * @returns true | false
     * @param {BaseClient} client
     */
    isRegistered(client:BaseClient);


    /**
     * Removes the client instance from the server. Called when client disconnects.
     * @param {BaseClient} client
     */
    unregister(client:BaseClient);

    /**
     * Returns all currently connected users.
     * @returns array[User]
     */
    getUserList();


    /**
     * Broadcasts an user update for the given user to all connected clients
     * @param {User} user
     */
    broadcastUserUpdate(user:User);

    userConnected(userID:number):boolean;

    getUser(userID:number):User;

    getClients(userID:number):BaseClient[];

    broadcastToClients(clients: BaseClient[], message:BaseMessage);

    createNewClient(clientSocket:any, server:TypeChatServer)

    lastClientofUserInRoom(client:BaseClient, roomID:number):boolean;
}
