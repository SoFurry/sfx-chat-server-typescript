/*
 * TypeChat NodeJS Chat Engine
 *
 * @link https://gitlab.com/terrabas/TypeChat
 * @copyright Copyright (c) 2018-2018 Martin Schwarz (TerraBAS) (http://mrtns.at)
 */

import {TypeChatServer} from "../TypeChatServer";

/**
 * Module constructor
 *
 * Every module should include a constructor which accepts TypeChatServer as the first argument.
 */
export interface BaseModuleConstructor {
    new(server:TypeChatServer):any;
}

/**
 * Base module interface
 *
 * Base interface for every server Module
 */
export interface BaseModuleInterface {

}


/**
 * The possible module's type. There can only be multiple "other" modules.
 */
export enum ModuleType {
    Auth,
    Client,
    Room,
    Permission,
    Other,
    None
}