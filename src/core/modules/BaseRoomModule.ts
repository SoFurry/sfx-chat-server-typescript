/*
 * TypeChat NodeJS Chat Engine
 *
 * @link https://gitlab.com/terrabas/TypeChat
 * @copyright Copyright (c) 2018-2018 Martin Schwarz (TerraBAS) (http://mrtns.at)
 */

import {BaseModuleInterface} from "./BaseModuleInterface";
import {Room} from "../../models/base/Room";
import {BaseClient} from "../../modules/base/BaseClient";
import {BaseMessage} from "../../models/base/BaseMessage";

export interface BaseRoomModule extends BaseModuleInterface {
    /**
     * Array that holds all rooms
     */
    rooms:Room[];

    /**
     * Function being executed on server initialisation.
     * Should fetch available rooms to join and load them into the rooms array.
     *
     * Must execute readyCallback when done.
     *
     * @param {() => void} readyCallback
     */
    fetchRooms(readyCallback:() => void)

    getDefaultRooms():Room[];

    getUserRooms():Room[];

    addClientToRoom(client:BaseClient, room:Room)

    exists(roomID:number):boolean;

    broadcastRoomMessage(message:BaseMessage, ignoredClient:BaseClient, store:boolean);

    getRoomClients(roomID:number):BaseClient[];

    updateRoom(room:Room):boolean;

    userLeaveRoom(userid:number,roomID:number):boolean;

}
