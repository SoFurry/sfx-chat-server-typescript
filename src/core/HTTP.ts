/*
 * TypeChat NodeJS Chat Engine
 *
 * @link https://gitlab.com/terrabas/TypeChat
 * @copyright Copyright (c) 2018-2018 Martin Schwarz (TerraBAS) (http://mrtns.at)
 */

import * as request from "request-promise";

export class HTTP {
    constructor() {

    }

    request(method: string, url: string, body: any, onSuccess: any, onFailure: any, headers: any) {
        if (headers == null)
            headers = {};
        headers = {...headers, 'User-Agent': 'TypeChat/v1', 'Accept': 'application/json'};
        request({
            uri: url,
            method: method,
            body: JSON.stringify(body),
            headers: headers
        }).then(function (parsedBody) {
            onSuccess(parsedBody);
        }).catch(function (err) {
            onFailure(err);
        });
    }

    requestJSON(method: string, url: string, body: any, onSuccess: any, onFailure: any, headers: any) {
        if (headers == null)
            headers = {};
        headers = {...headers, 'User-Agent': 'TypeChat/v1', 'Accept': 'application/json'};
        request({
            uri: url,
            method: method,
            body: JSON.stringify(body),
            json: true,
            headers: headers
        }).then(function (parsedBody) {
            onSuccess(parsedBody);
        }).catch(function (err) {
            onFailure(err);
        });
    }
}