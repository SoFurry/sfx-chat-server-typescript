/*
 * TypeChat NodeJS Chat Engine
 *
 * @link https://gitlab.com/terrabas/TypeChat
 * @copyright Copyright (c) 2018-2018 Martin Schwarz (TerraBAS) (http://mrtns.at)
 */

import {BaseAuthModule} from "./modules/BaseAuthModule";
import {ConsoleLog} from "../helpers/ConsoleLog";
import * as path from "path";
import {BaseModuleConstructor, ModuleType} from "./modules/BaseModuleInterface";
import {TypeChatServer} from "./TypeChatServer";
import {BaseClientModule} from "./modules/BaseClientModule";
import {BaseRoomModule} from "./modules/BaseRoomModule";
import * as fs from "fs";

export class ModuleManager {
    authModule: BaseAuthModule = null;
    clientModule: BaseClientModule = null;
    roomModule: BaseRoomModule = null;
    Logger: ConsoleLog;
    constructor(){
        this.Logger = new ConsoleLog("ModuleManager");
    }

    loadModulesFromModulesPath(server:TypeChatServer){
        fs.readdirSync(global['appRoot']+'/modules/').forEach(file => {
            file = file.substring(0,file.lastIndexOf('.'));
           this.loadModule(server, file);
        });
    }

    /**
     * Checks if all required modules have been loaded successfully
     * @returns {boolean}
     */
    checkModules(){
        return !(this.authModule == null || this.clientModule == null || this.roomModule == null);

    }

    loadModule(server:TypeChatServer, file:string){
        if(file === "")
            return;
        this.Logger.info("Loading module "+file+"...");
        const module: BaseModuleConstructor = require(path.join('../modules', file));
        try {
            const moduleType:ModuleType = module[file].moduleType;
            switch (moduleType){
                case ModuleType.Auth:
                    this.authModule = new module[file](server);
                    this.Logger.debug("Auth module loaded!");
                    break;
                case ModuleType.Client:
                    this.clientModule = new module[file](server);
                    this.Logger.debug("Client module loaded!");
                    break;
                case ModuleType.Room:
                    this.roomModule = new module[file](server);
                    this.Logger.debug("Room module loaded!");
                    break;
                case ModuleType.Other:
                    server[file] = new module[file](server);
                    this.Logger.debug("Additional module loaded!");
                    this.Logger.debug("Accessible via server."+file);
                    break;
                case ModuleType.None:
                    this.Logger.debug("Module set to none: Will not load!");
                    // We ignore this.
                    break;
                default:
                    this.Logger.error("Unimplemented module type! Module will not be loaded!");
            }
        } catch (e) {
            this.Logger.panic("Module load error");
            this.Logger.error(e.stack);
        }

    }

}