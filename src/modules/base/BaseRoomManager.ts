/*
 * TypeChat NodeJS Chat Engine
 *
 * @link https://gitlab.com/terrabas/TypeChat
 * @copyright Copyright (c) 2018-2018 Martin Schwarz (TerraBAS) (http://mrtns.at)
 */

import {BaseRoomModule} from "../../core/modules/BaseRoomModule";
import {ModuleType} from "../../core/modules/BaseModuleInterface";
import {ConsoleLog} from "../../helpers/ConsoleLog";
import {Room} from "../../models/base/Room";
import {TypeChatServer} from "../../core/TypeChatServer";
import * as _ from 'underscore';
import {BaseClient} from "./BaseClient";
import {ArrayHelper} from "../../helpers/ArrayHelper";
import {BaseMessage} from "../../models/base/BaseMessage";

export class BaseRoomManager implements BaseRoomModule {
    /**
     * Room Manager Module
     * @type {ModuleType.Room}
     */
    static moduleType: ModuleType = ModuleType.Room;

    Logger: ConsoleLog;
    rooms: Room[] = [];

    constructor(public server: TypeChatServer) {
        this.Logger = new ConsoleLog("RoomManager");
        this.Logger.debug("Module init!");
    }

    fetchRooms(readyCallback: any) {
        // Demo room
        this.rooms.push(new Room({
            id: 0,
            name: 'testroom',
            description: 'Test room',
            creator: 0,
            default_join: 1,
            memberRoles: null
        }));
        readyCallback();
    }

    getDefaultRooms() {
        return _.where(this.rooms, {default_join: 1});
    }

    getUserRooms(){
        return [];
    }

    getRoom(roomID: number): Room {
        return _.where(this.rooms, {id: roomID})[0];
    }

    getRoomClients(roomID: number): BaseClient[] {
        let clients = [];
        for (let user of this.getRoom(roomID).users) {
            this.Logger.debug("Found user: " + user.username);
            for (let client of this.server.moduleManager.clientModule.clients) {
                if (client.user.id == user.id) {
                    clients.push(client);
                    this.Logger.debug("Pushed user client");
                }
            }
        }
        return clients;
    }

    addClientToRoom(client: BaseClient, room: Room) {
        // Check if user is already in the list
        if (ArrayHelper.isEmpty(_.where(room.users, {id: client.user.id})))
            room.users.push(client.user);
    }

    broadcastRoomMessage(message: BaseMessage, ignoredClient:BaseClient, store:boolean) {
        let room = this.getRoom(message.destination.id);
        // TODO: Deletion of old messages
        if(store)
            this.rooms[this.getRoomIndex(room)].messages.push(message.storageMinify());

        for (let client of this.getRoomClients(message.destination.id)) {
            if(ignoredClient.clientSocket.id != client.clientSocket.id)
                client.sendMessage(message);
            else
                this.Logger.verbose("Skipping client "+ignoredClient.clientSocket.id+" for room message...");
        }
    }

    getRoomIndex(room:Room){
        return this.rooms.indexOf(room);
    }

    broadcastRoomUpdateMessage(room:Room){
        for (let client of this.getRoomClients(room.id)) {
            client.send("room update",room);
        }
    }

    updateRoom(room:Room):boolean{
        let oldRoom = this.getRoom(room.id);
        if(oldRoom == null)
            return false;
        room.messages = oldRoom.messages;
        room.users = oldRoom.users;
        // @ts-ignore
        let index = this.rooms.findIndex(x => x.id == room.id);
        this.rooms[index] = room;
        this.Logger.info("Room "+room.name+" has been updated. Broadcasting to clients...");
        this.broadcastRoomUpdateMessage(room);
        this.Logger.info("Room "+room.name+" client update complete.");
        return true;
    }

    getUserClientsFromRoom(userID:number, roomID:number): BaseClient[]{
        let room = this.getRoom(roomID);
        if(!room)
            return null;


        let clients = [];
        for (let user of this.getRoom(roomID).users) {
            if(user.id != userID)
                continue;
            for (let client of this.server.moduleManager.clientModule.clients) {
                if (client.user.id == user.id) {
                    clients.push(client);
                    this.Logger.debug("Pushed user client");
                }
            }
        }
        return clients;
    }

    userLeaveRoom(userid:number,roomID:number):boolean{
        // TODO: Performance can be improved here
        let clients = this.getUserClientsFromRoom(userid, roomID);
        if(clients == null)
            return false;
        for(let client of clients){
            client.leaveRoom(this.getRoom(roomID));
        }
        this.broadcastRoomUpdateMessage(this.getRoom(roomID));
        return true;
    }

    removeClientFromAllRooms(client: BaseClient) {
        // TODO Check if multiple clients with the same user are connected before removing the user!

        for (let room of this.rooms) {
            if (!ArrayHelper.isEmpty(_.where(room.users, {id: client.user.id}))) {
                ArrayHelper.remove(room.users, client.user);
            }
        }
    }

    exists(roomID: number): boolean {
        return !ArrayHelper.isEmpty(_.where(this.rooms, {id: roomID}));
    }

}