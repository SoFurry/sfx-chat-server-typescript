/*
 * TypeChat NodeJS Chat Engine
 *
 * @link https://gitlab.com/terrabas/TypeChat
 * @copyright Copyright (c) 2018-2018 Martin Schwarz (TerraBAS) (http://mrtns.at)
 */

import {BaseAuthModule} from "../../core/modules/BaseAuthModule";
import {ModuleType} from "../../core/modules/BaseModuleInterface";
import {ConsoleLog} from "../../helpers/ConsoleLog";
import {TypeChatServer} from "../../core/TypeChatServer";

export class BaseAuth implements BaseAuthModule{
    /**
     * SoFurry NEXT Auth module
     * @type {ModuleType.Auth}
     */

    static moduleType:ModuleType = ModuleType.Auth;

    Logger: ConsoleLog;

    constructor(public server: TypeChatServer) {
        this.Logger = new ConsoleLog("Auth");
        this.Logger.debug("Module init!");
    }

    authenticate(socketData: any, onSuccess: (userData) => void, onFailure: (reason) => void) {
        onSuccess({id:0,username:'testuser'});
    }
}