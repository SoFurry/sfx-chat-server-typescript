/*
 * TypeChat NodeJS Chat Engine
 *
 * @link https://gitlab.com/terrabas/TypeChat
 * @copyright Copyright (c) 2018-2018 Martin Schwarz (TerraBAS) (http://mrtns.at)
 */

import {BaseMessage} from "../../models/base/BaseMessage";
import {BaseClient} from "./BaseClient";
import {User} from "../../models/base/User";

/**
 * Checks message and user client permissions
 *
 * Function returns true: Access granted
 * Function returns false: Access denied
 */
export class BasePermissions {
    /**
     * Checked before message is being processed any further after receiving.
     * Should validate if sender ID (if given) matches up with the server's authentication info.
     */
    static preMessageProcessing(message:BaseMessage, sourceClient:BaseClient):boolean{
        if(message.source == null || message.source.id == null)
            return true;
        return message.source.id == sourceClient.user.id;
    }

    // noinspection JSUnusedLocalSymbols
    static canUserSendRoomMessage(user:User, roomID:number){
        return true;
    }

    // noinspection JSUnusedLocalSymbols
    static canUserSendWhisper(sourceUser:User, destinationUser:User){
        return true;
    }

}