/*
 * TypeChat NodeJS Chat Engine
 *
 * @link https://gitlab.com/terrabas/TypeChat
 * @copyright Copyright (c) 2018-2018 Martin Schwarz (TerraBAS) (http://mrtns.at)
 */

import {HTTP} from "../core/HTTP";
import {TypeChatServer} from "../core/TypeChatServer";
import {BaseAuth} from "./base/BaseAuth";

export class Auth extends BaseAuth{

    request: HTTP;

    constructor(public server: TypeChatServer) {
        super(server);
    }

}